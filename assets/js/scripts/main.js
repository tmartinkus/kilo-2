var selected = null;

function validateEmail() {
    var button = $('.email-form .btn');
    var email = $('#email');
    var acceptance = $('#acceptance');
    var err = false;
    var emailRegex = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

    if (!email.val() || !emailRegex.test(email.val())) {
        err = true;
        email.parent().addClass('is-invalid');
    }

    if (!acceptance.is(':checked')) {
        err = true;
        acceptance.parents('.form-group').addClass('is-invalid');
    }

    if (err) {
        shake(button);
    }

    return !err;
}

$('input').focus(function () {
    $(this).parents('.form-group').removeClass('is-invalid');
});
$('.checkbox-container').click(function () {
    $(this).parents('.form-group').removeClass('is-invalid');
});

function shake(el) {
    el.addClass('animation-shake');
    setTimeout(function () {
        el.removeClass('animation-shake');
    }, 450);
}

if ($('.questionnaire > .btn').get(0)) {
    $('.questionnaire > .btn').click(function () {
        $('.selected').removeClass('selected');
        selected = $(this);
        selected.addClass('selected');
    });

    $('.questionnaire-footer .btn').click(function () {
        if (selected === null) {
            shake($('.questionnaire > .btn.btn--secondary'));
        } else {
            // Do something
        }
    });
}

if ($('#range').get(0)) {
    selected = 4;
    $("#range").on('mousemove', updateRangeBackground).on('touchmove', updateRangeBackground);

    function updateRangeBackground(e) {
        var val = ($(this).val() - $(this).attr('min')) / ($(this).attr('max') - $(this).attr('min'));
        var percent = val * 100;

        $(this).css('background-image', '-webkit-gradient(linear, left top, right top, ' + 'color-stop(' + percent + '%, #4259C2), ' + 'color-stop(' + percent + '%, #8C9DE6)' + ')');
        $(this).css('background-image', '-moz-linear-gradient(left center, #4259C2 0%, #4259C2 ' + percent + '%, #8C9DE6 ' + percent + '%, #8C9DE6 100%)');
        $(this).css('background-image', 'linear-gradient(to right, #4259C2 0%, #4259C2 ' + percent + '%, #8C9DE6 ' + percent + '%, #8C9DE6 100%);');

        $('#slider-hours').text($(this).val());
        $('#slider-income').text($(this).val() * 300);
        $('.hour-container .hour').removeClass('active');
        $('.hour-container .hour.hour-' + $(this).val()).addClass('active');
        selected = $(this).val();
    }
}

if ($('#loader')) {
    var end = 4000;
    var percent = 0;

    add();

    function add() {
        setTimeout(function () {
            percent++;

            $('#loader-progress').css('right', 100 - percent + '%');
            $('#loader-percent').text(percent + '%');

            if (percent !== 100) {
                add();
            } else {
                // Do something
            }
        }, end / 100)
    }
}

if ($('.radio-group').get(0)) {
    $('.radio-container').on('click', function () {
        $(this).siblings().removeClass('checked');
        $(this).addClass('checked');
    });

    $('.questionnaire-footer .btn').on('click', function () {
        var gender = $('#gender input[type=radio]:checked').val();
        var age = $('#age input[type=radio]:checked').val();
        var city = $('#city input[type=radio]:checked').val();

        var err = false;
        if (!gender) {
            shake($('#gender'));
            err = true;
        }
        if (!age) {
            shake($('#age'));
            err = true;
        }
        if (!city) {
            shake($('#city'));
            err = true;
        }

        if (!err) {
            // Do something
        }
    });
}

if ($('.accordion').get(0)) {
    $('.accordion').on('click', function () {
        $(this).toggleClass('expanded')
    });
}

if ($('.slider').get(0)) {
    $(document).ready(function () {
        $('.swiper-container').each(function () {
            console.log($(this).attr('id'));
            var swiper = new Swiper(('#' + $(this).attr('id')), {
                autoHeight: true,
                spaceBetween: 15,
                slidesPerView: 'auto',
                loop: true,
                centeredSlides: true,
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                }
            });
        });
    });
}

if ($('.timer').get(0)) {
    const eventTime = new Date().getMilliseconds() + 1.9 * (24 * 60 * 60) + 2; // Timestamp - Sun, 21 Apr 2013 13:00:00 GMT
    const currentTime = new Date().getMilliseconds(); // Timestamp - Sun, 21 Apr 2013 12:30:00 GMT
    const diffTime = eventTime - currentTime;
    var duration = moment.duration(diffTime * 1000, 'milliseconds');
    const interval = 1000;

    setInterval(function () {
        duration = moment.duration(duration - interval, 'milliseconds');
        $('#timer-days').text(duration.days());
        $('#timer-hours').text(duration.hours());
        $('#timer-minutes').text(duration.minutes());
        $('#timer-seconds').text(duration.seconds());
    }, interval);
}

if ($('.side-nav').get(0)) {
    $('.burger-button').on('click', function () {
        $('.side-nav').addClass('shown');
        setTimeout(function () {
            $('.side-nav-cover').fadeIn();
        }, 150);
    });
    $('.close').on('click', function () {
        $('.side-nav').removeClass('shown');
        $('.side-nav-cover').fadeOut();
    });

    $('.side-nav .btn').on('click', function () {
        var id = $(this).attr('data-id');
        const position = $('#' + id).offset().top;
        console.log(position);
        $("HTML, BODY").animate({scrollTop: position - 80}, 1000);
        $('.side-nav').removeClass('shown');
        $('.side-nav-cover').fadeOut();
    });

    $('.header .nav .link').on('click', function () {
        var id = $(this).attr('data-id');
        const position = $('#' + id).offset().top;
        $("HTML, BODY").animate({scrollTop: position - 150}, 1000);
    });
}